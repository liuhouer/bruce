<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="zh-CN"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
<meta name="apple-mobile-web-app-status-bar-style" content="black">

<meta http-equiv="Content-Language" content="zh-CN">

<meta name="author" content="www.qinco.net">
<meta name="robots" content="index,follow,archive">
<link rel="shortcut icon" href="img/favicon.png">
<link rel="stylesheet" href="/css/ace/css/bootstrap.min.css" />
		<link rel="stylesheet" href="/css/ace/css/font-awesome.min.css" />

		<!-- page specific plugin styles -->

		<!-- text fonts -->
		<link rel="stylesheet" href="/css/ace/css/ace-fonts.css" />

		<!-- ace styles -->
		<link rel="stylesheet" href="/css/ace/css/ace.min.css" id="main-ace-style" />

		<!--[if lte IE 9]>
			<link rel="stylesheet" href="/css/ace/css/ace-part2.min.css" />
		<![endif]-->
		<link rel="stylesheet" href="/css/ace/css/ace-skins.min.css" />
		<link rel="stylesheet" href="/css/ace/css/ace-rtl.min.css" />

		<!--[if lte IE 9]>
		  <link rel="stylesheet" href="/css/ace/css/ace-ie.min.css" />
		<![endif]-->

		<!-- inline styles related to this page -->

		<!-- ace settings handler -->
		<script src="/css/ace/js/ace-extra.min.js"></script>

<title>布.词故事::</title>
<meta name="description" content="布.词故事::第1页::布.词会让您记住每一件美好的事物，正是它们勾勒出了您最真实的生命轨迹.分享好东西，记录生命回忆，记住世界上最好的东西。">
<meta name="keywords" content="最爱,回忆,生活">
<%@ include file="/page/common/common.jsp"%>

<body style="">

	<%@ include file="/page/common/navigation.jsp"%>
	 
	
<div class="clearfix maincontent" style="background:#f4f3f1">
	<div class="container">
		<div class="mainbody" style="margin-top:80px; ">
			<div class="row">
				
					<c:forEach items="${list }" var="s" varStatus="ss">
						<c:forEach items="${UList }" var="u" varStatus="uu">
								 <c:if test="${u.id==s.userid }">
								
								
								
								<div class="timeline-container">
												<div class="timeline-label">
													<!-- #section:pages/timeline.label -->
													<span class="label label-primary arrowed-in-right label-lg">
														<b>${s.createtime }</b>
													</span>

													<!-- /section:pages/timeline.label -->
												</div>

												<div class="timeline-items">
													<!-- #section:pages/timeline.item -->
													<div class="timeline-item clearfix">
														<!-- #section:pages/timeline.info -->
														<div class="timeline-info">
															<img alt="${u.username}Avatar" 
															<c:if test="${u.headpath ==null}">src="/img/davatar.jpg"</c:if>
															<c:if test="${u.headpath !=null}"><img 
															 <c:choose>
				   												<c:when test="${fn:contains(u.headpath ,'http://') }">src="${u.headpath }"</c:when>
				                                  				<c:otherwise>src="bruce/${u.headpath }"</c:otherwise>
				                                			</c:choose> 
															
															 </c:if>
															>
															<span class="label label-info label-sm"><!-- 16:22 --></span>
														</div>

														<!-- /section:pages/timeline.info -->
														<div class="widget-box transparent">
															<div class="widget-header widget-header-small">
																<h5 class="widget-title smaller">
																	<a href="#" class="blue">${u.username}</a>
																	<span class="grey">Written</span>
																</h5>

																<!-- <span class="widget-toolbar no-border">
																	<i class="ace-icon fa fa-clock-o bigger-110"></i>
																	16:22
																</span>

																<span class="widget-toolbar">
																	<a href="#" data-action="reload">
																		<i class="ace-icon fa fa-refresh"></i>
																	</a>

																	<a href="#" data-action="collapse">
																		<i class="ace-icon fa fa-chevron-up"></i>
																	</a>
																</span> -->
															</div>

															<div class="widget-body">
																<div class="widget-main">
																	<!-- <span class="red">high life</span> -->

																	${s.note }
																	<div class="space-6"></div>

																	<!-- <div class="widget-toolbox clearfix">
																		<div class="pull-left">
																			<i class="ace-icon fa fa-hand-o-right grey bigger-125"></i>
																			<a href="#" class="bigger-110">Click to read …</a>
																		</div>

																		#section:custom/extra.action-buttons
																		<div class="pull-right action-buttons">
																			<a href="#">
																				<i class="ace-icon fa fa-check green bigger-130"></i>
																			</a>

																			<a href="#">
																				<i class="ace-icon fa fa-pencil blue bigger-125"></i>
																			</a>

																			<a href="#">
																				<i class="ace-icon fa fa-times red bigger-125"></i>
																			</a>
																		</div>

																		/section:custom/extra.action-buttons
																	</div> -->
																</div>
															</div>
														</div>
													</div>

													<!-- /section:pages/timeline.item -->
									</div>
								
								
								
					</c:if>
					 </c:forEach>
					</c:forEach>
					
						 

									 

			

								 
				  	
						 
					
		  	</div>

		  	
		</div>
	</div>
</div>
	 

	<%@ include file="/page/common/container.jsp"%>

    




<script type="text/javascript">

$(document).ready(function() {

	var ajax_url='/ajax';
	var _aj = {user_id: '50777'};
	_aj['user_agent']='68A697E775AE';
	_aj['timestamp']='1400553738';
	_aj['user_keychain']='CBBDECB98732';

	
	});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-199262-13', 'buci.cc');
  ga('send', 'pageview');

</script>

 


<script type="text/javascript" charset="utf-8" id="ABD75F83F0359849_Analytics" src="http://tajs.qq.com/stats?sId=26628622"></script></body></html>