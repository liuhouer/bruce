
package com.bruce.action;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.multipart.MultipartFile;

import com.bruce.manager.UserManager;
import com.bruce.manager.UserprofileManager;
import com.bruce.model.QQinfo;
import com.bruce.model.User;
import com.bruce.model.Userprofile;
import com.bruce.query.UserQuery;
import com.bruce.utils.Base64Util;
import com.bruce.utils.Timers;
import com.bruce.utils.json.JsonUtil;

@Controller
@RequestMapping("/cm")
@SessionAttributes({ "list", "User" })
public class UserAction {

	 private final String LIST_ACTION = "redirect:/cm/list";
	 private final String COMMON_PIC = "redirect:/cm/pic";
	 private final String DOMAIN = "lonelyrobots.cn";
	 @Autowired	
	 private UserManager userManager;
	 @Autowired	
	 private UserprofileManager userprofileManager;
	 @Autowired	
	 private UserQuery userQuery;
	 
	 @RequestMapping("/xbjt")
		public String xbjt(ModelMap map) {
			return "/page/bruce-quiet-listen";
		}
	 
	 @RequestMapping("/index")
		public String index(ModelMap map) {
			//List<User> Pidlist = userManager.findAll();
			//map.addAttribute("Pidlist",Pidlist);
			return "/page/logIn";
		}
	 
	   /* //一键修改密码
	    @RequestMapping("/pwddd")
	 	@ResponseBody
		public String pwddd() {
	 		String msg = "success";
	 		List<User> list = userManager.findByCondition(" where 1=1").getResultlist();
	 		for (int i = 0; i < list.size(); i++) {
				String pwd=  list.get(i).getPassword();
				String encodepwd= Base64Util.JIAMI(pwd);
				String id  = list.get(i).getId();
				String sql = "update bc_user a  set a.password = '"+encodepwd+"' where a.id = '"+id+"' ";
				userManager.pwddd(sql);
			}
	 		
			return msg;
		}*/
		@RequestMapping("/toEditInfo")
		public String toEditInfo(ModelMap map,String userid,HttpServletRequest request) {
			if(StringUtils.isEmpty(userid)){
	              User u = (User) request.getSession().getAttribute("user");
	              userid = u.getId();
	        }  
			
			User user = userManager.findUser(userid);
			//处理图片路径
				String imgpath = user.getHeadpath(); //e:/yunlu/upload/1399976848969.jpg
				if(!StringUtils.isEmpty(imgpath)){
				String[] str = imgpath.split("/heads/");
				if(str.length>1){
				String imgp = "heads/"+str[1];
				user.setHeadpath(imgp);
				}
				}
				//处理图片路径
			map.put("MyInfo", user);
			Userprofile Duser = userprofileManager.getModelByUserid(userid);
			map.put("Dinfo", Duser);
			return "EditInfo";
		}
		
		@RequestMapping("/saveEditInfo")
		public String saveEditInfo(User model,ModelMap map,String oldpath, @RequestParam(value = "file", required = false) MultipartFile[] file,String new_password,String new_password_confirmation,String courseware,String year_of_birth,String user_slug) {
			//保存User表信息
			// 执行删除图片缓存
			if(file.length>=1){
				System.out.println(file[0].getOriginalFilename()+"------------------------------------------------》》"); 
				if(StringUtils.isNotEmpty(file[0].getOriginalFilename())){//新上传了图片才把以前的删除
			if(StringUtils.isNotEmpty(oldpath)){
			  File f = new File(oldpath);
			  System.out.println("要删除文件的绝对路径是："+f.getAbsolutePath());
				if (f.exists()){
					f.delete();
				}else{
					System.out.println("文件已经丢失!");
				}
			  }
			}
			}
			// 执行删除图片缓存
		    //执行上传头像	
			 System.out.println("-------------------------------------->开始");  
			    Properties prop = System.getProperties();

				String os = prop.getProperty("os.name");
				System.out.println(os);
				String path = "c:/bruce/album";
				String fileName="";
				String newName="";
		         if(os.startsWith("win") || os.startsWith("Win") ){// windows操作系统
		        	 path = "c:/bruce/heads";
		         }else{
		        	 path = "/mnt/apk/heads";
		         }
		       //  String path = "c:/bruce/upload";
		         String headpath = "";
		        for (int i = 0; i < file.length; i++) {
		        	fileName = file[i].getOriginalFilename();  
			        System.out.println(path); 
			        if(!StringUtils.isEmpty(fileName)){//新上传了才执行保存
			        String ext = fileName.substring(fileName.lastIndexOf(".")+1,fileName.length()); 
			        newName = String.valueOf(System.currentTimeMillis())+"."+ext;
			        File targetFile = new File(path, newName);  
			        if(!targetFile.exists()){  
			            targetFile.mkdirs();  
			        }  
			  
			        //保存  
			        try {  
			            file[i].transferTo(targetFile);  
			        } catch (Exception e) {  
			            e.printStackTrace();  
			        }  
			        
			        if(i==0){
			        	headpath = newName;
			        }
			        }
				}
//		        String imgpath = path+"/"+newName;
//		        System.out.println(imgpath);
		      //执行上传头像end
		      //上传图片非空时，执行保存路径
		        headpath = path+"/"+headpath;
		        //lrcpath = path +"/"+lrcpath;
		        if(headpath.contains(".")){
		        	model.setHeadpath(headpath);
		        }
		        System.out.println("-------------------------------------->结束");  
		        
		        //处理密码信息
		        if(!StringUtils.isEmpty(new_password)&&!StringUtils.isEmpty(new_password_confirmation)&&new_password.equals(new_password_confirmation)){
		        	model.setPassword(new_password);
		        }
		        //处理密码信息
			   this.userManager.updateUser(model);
			  //保存User表信息-------结束
			   
			 //保存User明细表信息-------start
            String userid = model.getId();
            Userprofile up = userprofileManager.getModelByUserid(userid);
            if(StringUtils.isEmpty(up.getUser_id())){
            	up.setUser_id(userid);
            }
            if (!StringUtils.isEmpty(courseware)) {
            	up.setCourseware(courseware);
			}
            if (!StringUtils.isEmpty(year_of_birth)) {
            	up.setYear_of_birth(year_of_birth);
			}
            if (!StringUtils.isEmpty(user_slug)) {
            	up.setUser_slug(user_slug);
			}
            userprofileManager.updateUserprofile(up);
          //保存User明细表信息-------end
			return "redirect:toMyself?userid="+userid;
		}
		
		@RequestMapping("/toLogin")
		public String toLogin(ModelMap map) {
			//List<User> Pidlist = userManager.findAll();
			//map.addAttribute("Pidlist",Pidlist);
			return "login";
		}
		
		@RequestMapping("/toEdit")
		public String toEdit(HttpServletRequest request, String id,ModelMap map) {
			if(StringUtils.isEmpty(id)){
                User u = (User) request.getSession().getAttribute("user");
                id = u.getId();
            }  
			if(!StringUtils.isEmpty(id)){
				User model = userManager.findUser(id);
				map.put("model", model);
			}
			return "user/userEdit";
		}
		
		@RequestMapping("/remove")
		public String remove(@RequestParam("id") String id) {
			this.userManager.delUser(id);
			return LIST_ACTION;
		}
		
		@RequestMapping("/removes")
		public String removes(@RequestParam("ids") String ids) {
			String[] str = ids.split(",");
			for(String s :str){
				this.userManager.delUser(s);
			}
			return LIST_ACTION;
		}
		
		@RequestMapping("/update")
		public String update(User model) {
			this.userManager.updateUser(model);
			return LIST_ACTION;
		}
		
		@RequestMapping("/logout")
		public String logout(HttpServletRequest request, HttpSession session,HttpServletResponse response) {

			Enumeration e = session.getAttributeNames();
			while (e.hasMoreElements()) {
				String sessionName = (String) e.nextElement();
				System.out.println("存在的session有：" + sessionName);
					session.removeAttribute(sessionName);

			}
			clearCookie(request, response);
			String a = request.getParameter("flag");
			String etc="?signout=true";
			if(StringUtils.isNotEmpty(a)){
				if(a.equals("qq")){
					etc = "";
				}
			}
			
			return LIST_ACTION+etc;
		}
		
		

		/**
		 * 清空cookie,保留username
		 */
		private static void clearCookie(HttpServletRequest request,
				HttpServletResponse response) {
			Cookie[] cookies = request.getCookies();
			try {
				for (int i = 0; i < cookies.length; i++) {
						Cookie cookie = new Cookie(cookies[i].getName(), null);
						cookie.setMaxAge(0);
						response.addCookie(cookie); 
				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		}

		@RequestMapping("/addUser")
		public String addUser(User user, ModelMap map,HttpSession session) {
			int num = userManager.findByCondition(" where email = '"+user.getEmail()+"' ").getResultlist().size();
			if(num>0){
				map.put("reged", "reged");
				return "reg";
			}else{
				
				user.setDate_joined(Timers.nowTime());
				String username ="";
				String str = user.getEmail();
				if(str.contains("@")){
					String[] strs = str.split("@");
					username=strs[0];
				}else{
					username=str;
				}
				user.setUsername(username);
				user.setPassword(Base64Util.JIAMI(user.getPassword()));
				this.userManager.addUser(user);
				session.setAttribute("user", user);
				map.put("user", user);
				return "redirect:pic";
			}
		}

		@RequestMapping("/findUser")
		private String findUser(@RequestParam("id") String id, ModelMap map) {
			User user = this.userManager.findUser(id);
			map.addAttribute("user", user);
			return "findresult";
		}

		@RequestMapping("/delUser")
		public String delUser(@RequestParam("id") String id) {
			this.userManager.delUser(id);
			return LIST_ACTION;
		}

		@RequestMapping("/updateUser")
		public String updateUser(@RequestParam("id") String id) {
			User user = this.userManager.findUser(id);
			this.userManager.updateUser(user);
			return LIST_ACTION;
		}

		
		
		
	@RequestMapping("/login")
	@ResponseBody
	public String login(HttpServletRequest request,
			HttpServletResponse response, HttpSession session, String email,
			String password, ModelMap map) throws IOException {
		String result = "success";
		String info   = "";
		Cookie[] cookies = request.getCookies();
		if (!StringUtils.isEmpty(email) && !StringUtils.isEmpty(password)) {
			password = Base64Util.JIAMI(password);
			User user = userManager.login(email, password);
			if (user != null) {
				//处理头像

			    String imgpath = user.getHeadpath();
                if(!StringUtils.isEmpty(imgpath)){
                   String[] str = imgpath.split("/heads/");
                   if(str.length>1){
                   String imgp = "heads/"+str[1];
                   user.setHeadpath(imgp);
                   }
                }

				session.setAttribute("user", user);
				map.put("user", user);
				Cookie cname = new Cookie("email", email);
				Cookie cpasswd = new Cookie("password", password);

				/* cookie的有效期为30秒 */
				cname.setPath("/");
				cname.setDomain(DOMAIN);
				cname.setMaxAge(60 * 60 * 24 * 7); /* 设置cookie的有效期为 7 天 */
				cpasswd.setPath("/");
				cpasswd.setDomain(DOMAIN);
				cpasswd.setMaxAge(60 * 60 * 24 * 7); /* 设置cookie的有效期为 7 天 */
				response.addCookie(cname);
				response.addCookie(cpasswd);
				session.setAttribute("user", user);
				result = "success";
				info = "登陆成功";
			} else {
				result = "error";
				info = "用户名密码错误";
			}
		} else if (cookies != null) {
			for (int i = 0; i < cookies.length; i++) {
				if (cookies[i].getName().equals("email")) {
					String name = cookies[i].getValue();
					if (!StringUtils.isEmpty(name)) {
						email = name;
					}
					continue;
				}

				if (cookies[i].getName().equals("password")) {
					String password1 = cookies[i].getValue();
					if (!StringUtils.isEmpty(password1)) {
						password = password1;
					}
					continue;
				}
			}
			User user = userManager.login(email, password);
			if (user != null) {
				//处理头像

			    String imgpath = user.getHeadpath();
                if(!StringUtils.isEmpty(imgpath)){
                   String[] str = imgpath.split("/heads/");
                   if(str.length>1){
                   String imgp = "heads/"+str[1];
                   user.setHeadpath(imgp);
                   }
                }

				session.setAttribute("user", user);
				map.put("user", user);

				result = "success";
				info = "登陆成功";
			}else {
				result = "error";
				info = "用户名密码错误";
			}
		}
		return result;
	}
	
	
	/**
	 * 绑定注册/登录用户信息
	 */
	@RequestMapping(value = "/qq/add" )
	@ResponseBody
	public String qqAdd(HttpServletRequest request, HttpServletResponse response, HttpSession session, String openId,String qqinfo) {
		String result = "success";
		System.out.println(openId);
		if(StringUtils.isEmpty(openId)){
			openId = (String) session.getAttribute("openId");
		}
		List<User> list = userManager.findByCondition(" where qq_openid = '"+openId+"' ").getResultlist();
		if(list!=null){
			if(list.size()==0){

				System.out.println(qqinfo);
				if(StringUtils.isNotEmpty(qqinfo)){
					QQinfo qqinfom = JsonUtil.jsonToModel(qqinfo, QQinfo.class);
					//添加user
					User userm = new User();
					userm.setDate_joined(Timers.nowTime());
					userm.setHeadpath(qqinfom.getFigureurl_qq_2());
					userm.setIs_active(1);
					userm.setIs_del(0);
					userm.setLast_login(Timers.nowTime());
					userm.setQq_info(qqinfo);
					userm.setQq_openid(openId);
					userm.setUsername("qq_"+qqinfom.getNickname());
					userManager.addUser(userm);
					//添加详情
					Userprofile upm=new Userprofile();
					upm.setCity(qqinfom.getCity());
					upm.setGender(qqinfom.getGender());
					upm.setYear_of_birth(qqinfom.getYear());
					upm.setUser_id(userm.getId());
					userprofileManager.addUserprofile(upm);
				}
				
				
				
			}else{
				session.setAttribute("user", list.get(0));
			}
		}
		
		return result;
	}
	
	/**
	 * 判断登录用户信息【存在登陆成功，取出用户信息】【不存在，存放qqopenid，等待绑定信息】
	 */
	@RequestMapping(value = "/qq/flag" )
	@ResponseBody
	public String qqFlag(HttpServletRequest request, HttpServletResponse response, HttpSession session, String openId) {
		String result = "0";
		System.out.println(openId);
		List<User> list = userManager.findByCondition(" where qq_openid = '"+openId+"' ").getResultlist();
		session.setAttribute("openId", openId);
		if(list!=null){
			if(list.size()>0){
				User user = list.get(0);
				session.setAttribute("user", user);
				result = "1";
			}
		}
		return result;
	}
	

    

}
