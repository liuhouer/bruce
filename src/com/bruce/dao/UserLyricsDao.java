package com.bruce.dao;

import java.io.Serializable;


import com.bruce.model.UserLyrics;

public interface UserLyricsDao extends HibernateDao<UserLyrics, Serializable> {
	
}