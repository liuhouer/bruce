package com.bruce.dao;

import java.io.Serializable;


import com.bruce.model.UserFollow;

public interface UserFollowDao extends HibernateDao<UserFollow, Serializable> {
	
}