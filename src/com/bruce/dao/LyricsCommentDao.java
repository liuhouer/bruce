package com.bruce.dao;

import java.io.Serializable;


import com.bruce.model.LyricsComment;

public interface LyricsCommentDao extends HibernateDao<LyricsComment, Serializable> {
	
}