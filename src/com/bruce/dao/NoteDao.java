package com.bruce.dao;

import java.io.Serializable;


import com.bruce.model.Note;

public interface NoteDao extends HibernateDao<Note, Serializable> {
	
}