package com.bruce.dao;

import java.io.Serializable;


import com.bruce.model.Lyrics;

public interface LyricsDao extends HibernateDao<Lyrics, Serializable> {
	
}