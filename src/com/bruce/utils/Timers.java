package com.bruce.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Timers {
	  private static final long ONE_MINUTE = 60000L;  
      private static final long ONE_HOUR = 3600000L;  
      private static final long ONE_DAY = 86400000L;  
      private static final long ONE_WEEK = 604800000L;  
      
      private static final String ONE_SECOND_AGO = "秒前";  
      private static final String ONE_MINUTE_AGO = "分钟前";  
      private static final String ONE_HOUR_AGO = "小时前";  
      private static final String ONE_DAY_AGO = "天前";  
      private static final String ONE_MONTH_AGO = "月前";  
      private static final String ONE_YEAR_AGO = "年前";  
	 /**@author bruce
	  * 取得当前时间 格式yyyy-MM-dd hh:mm:ss
	  */
		public static String nowTime() {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(new Date().getTime());
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd HH:mm:ss");
			return dateFormat.format(c.getTime());
		}
		
		/**@author bruce
		  * 取得当前日期
		  */
		public static String nowdate() {
			Calendar c = Calendar.getInstance();
			c.setTimeInMillis(new Date().getTime());
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"yyyy-MM-dd");
			return dateFormat.format(c.getTime());
		}
		
		/**@author bruce
		  * 取得N个月以后的日期
		  */
		public static String N_MonthDate(int n ) {
			Calendar c = Calendar.getInstance();
			c.add(c.MONTH, n);
			return ""+c.get(c.YEAR)+"-"+(c.get(c.MONTH)+1)+"-"+c.get(c.DATE);
		}
		
		/**@author bruce
		 * @function 取得标准时间 2014-05-23从格式yyyy-MM-dd hh:mm:ss
		 */
		
		public static String getHalfDate(String timeStr) {
			String t= timeStr;
			if(timeStr.contains("-")){
				 t = timeStr.substring(0, 10);
			}
			return t;
		}
		
		/**@author bruce
		 * @function 取得标准时间 2014从格式yyyy-MM-dd hh:mm:ss
		 */
		
		public static String getYear(String timeStr) {
			String t= timeStr;
			if(timeStr.contains("-")){
				 t = timeStr.substring(0, 4);
			}
			return t;
		}
		
		public static void main(String[] args) {
			System.out.println(N_MonthDate(5));
		}
		
		//格式化时间串成为  几天前 几秒前 几小时前  几分钟前 几年前sth.....
		public static String formatToNear(String str){
			
				try {
					SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:m:s");  
			        Date date;
					date = format.parse(str);
					str = Timers.format(date);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			return str;
			
		}
		
		//格式化时间串成为  几天前 几秒前 几小时前  几分钟前 几年前sth.....
		public static String format(Date date) {  
	        long delta = new Date().getTime() - date.getTime();  
	        if (delta < 1L * ONE_MINUTE) {  
	            long seconds = toSeconds(delta);  
	            return (seconds <= 0 ? 1 : seconds) + ONE_SECOND_AGO;  
	        }  
	        if (delta < 45L * ONE_MINUTE) {  
	            long minutes = toMinutes(delta);  
	            return (minutes <= 0 ? 1 : minutes) + ONE_MINUTE_AGO;  
	        }  
	        if (delta < 24L * ONE_HOUR) {  
	            long hours = toHours(delta);  
	            return (hours <= 0 ? 1 : hours) + ONE_HOUR_AGO;  
	        }  
	        if (delta < 48L * ONE_HOUR) {  
	            return "昨天";  
	        }  
	        if (delta < 30L * ONE_DAY) {  
	            long days = toDays(delta);  
	            return (days <= 0 ? 1 : days) + ONE_DAY_AGO;  
	        }  
	        if (delta < 12L * 4L * ONE_WEEK) {  
	            long months = toMonths(delta);  
	            return (months <= 0 ? 1 : months) + ONE_MONTH_AGO;  
	        } else {  
	            long years = toYears(delta);  
	            return (years <= 0 ? 1 : years) + ONE_YEAR_AGO;  
	        }  
	    }  
	  
	    private static long toSeconds(long date) {  
	        return date / 1000L;  
	    }  
	  
	    private static long toMinutes(long date) {  
	        return toSeconds(date) / 60L;  
	    }  
	  
	    private static long toHours(long date) {  
	        return toMinutes(date) / 60L;  
	    }  
	  
	    private static long toDays(long date) {  
	        return toHours(date) / 24L;  
	    }  
	  
	    private static long toMonths(long date) {  
	        return toDays(date) / 30L;  
	    }  
	  
	    private static long toYears(long date) {  
	        return toMonths(date) / 365L;  
	    }  
		
		
}
