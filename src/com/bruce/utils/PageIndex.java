package com.bruce.utils;

/**
 * 计算首页和尾页的页码
 * @author Administrator
 *
 */
public class PageIndex {
	private int startindex;
	private int endindex;
	
	public PageIndex() {
		// TODO Auto-generated constructor stub
	}
	
	public int getStartindex() {
		return startindex;
	}
	public void setStartindex(int startindex) {
		this.startindex = startindex;
	}
	public int getEndindex() {
		return endindex;
	}
	public void setEndindex(int endindex) {
		this.endindex = endindex;
	}
	 
}
